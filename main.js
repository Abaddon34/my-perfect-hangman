$(document).ready(function() {
});

const canvas = document.getElementById('penduCanvas');
const pendu = canvas.getContext("2d");

let lettres = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];

let $words = ["BOUTEILLE", "CEREALES", "PONEY", "COUSSIN", "ECOUTEURS", "CASSEROLE", "FEUILLE", "CAFETIERE", "TELEPHONE", "SOPALIN", "LUNETTES"]
let $bouton; 
let $lettre;
let $boolOfWordFinded = false;
let $numberIncrementPendu = 0;
let $numberIncrementVictory = 0;

function Win(){ 
  alert('Vous avez gagner ! félicitation, une nouvelle partie commencera quand vous cliquerez sur OK.');
  setTimeout(reload(), 99999999999);
}

function Lose(){ 
  alert('Vous avez perdu, la partie recommencera quand vous cliquerez sur OK.');
  setTimeout(reload(), 99999999999);
}

function reload(){
  location.reload();
}

$wordNumber = Math.ceil(Math.random() * (0 + 10) + 0);
$wordSplitted = $words[$wordNumber].split('')

for (let i = 0; i < lettres.length; i++) {
  $('<button/>', { 
    text: lettres[i],
    id: 'btn_' + lettres[i], 
    width: "30px", 
    click: function (event) { 
      onClick(event, false); 
    } 
  }).appendTo("#lettres"); 
}

for ( let y = 0; y < $wordSplitted.length; y++) {
  $('<p/>', {
    text: '_',
    id: 'sm_' + y,
    width: "30px"
  }).appendTo("#mot") 
}

function onClick(event,isKeyPress){ 
  $bouton = $(event.target); 
  $($bouton).attr("disabled", "disabled"); 
  $lettre = $($bouton).text();
  $boolOfWordFinded = false;
  for ( let j = 0; j < $wordSplitted.length; j++) {
    if($wordSplitted[j] == $lettre) {
      $("#sm_" + j).text($lettre);
      $boolOfWordFinded = true;
      $numberIncrementVictory = $numberIncrementVictory + 1;
      if($numberIncrementVictory >= $wordSplitted.length)
      {
        Win();
      }
    }
  }
  if($boolOfWordFinded == false) {
    dessinPendu($numberIncrementPendu);
  }
}

function dessinPendu(number){
  switch(number) 
  {
   case 0: 
    pendu.lineWidth = 3;
    pendu.beginPath();
    pendu.moveTo(175, 225);
    pendu.lineTo(5, 225);
    pendu.moveTo(30, 225);
    pendu.lineTo(25, 5);
    pendu.lineTo(100, 5);
    pendu.lineTo(100, 25);
    pendu.stroke();
   break; 
   case 1:
    pendu.beginPath();
    pendu.arc(100, 50, 25, 0, Math.PI*2, true);
    pendu.closePath();
    pendu.stroke(); 
   break; 
   case 2: 
    pendu.beginPath();
    pendu.moveTo(100, 75);
    pendu.lineTo(100, 140);
    pendu.stroke();
   break; 
   case 3: 
    pendu.beginPath();
    pendu.moveTo(100, 85);
    pendu.lineTo(60, 100);
    pendu.stroke();
   break; 
   case 4: 
    pendu.beginPath();
    pendu.moveTo(100, 85);
    pendu.lineTo(140, 100);
    pendu.stroke(); 
   break; 
   case 5: 
    pendu.beginPath();
    pendu.moveTo(100, 140);
    pendu.lineTo(80, 190);
    pendu.stroke();
   break; 
   case 6: 
    pendu.beginPath();
    pendu.moveTo(100, 140);
    pendu.lineTo(125, 190);
    pendu.stroke();
   break;
   default:
   Lose();
   } 
  $numberIncrementPendu = $numberIncrementPendu + 1;
}
